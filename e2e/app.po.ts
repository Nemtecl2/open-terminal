import { browser, element, by } from 'protractor';

/* tslint:disable */
export class OpenTerminalPage {
  navigateTo(route: string) {
    return browser.get(route);
  }
}
