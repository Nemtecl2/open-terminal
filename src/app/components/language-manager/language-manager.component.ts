import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-language-manager',
  templateUrl: './language-manager.component.html',
  styleUrls: ['./language-manager.component.scss']
})
export class LanguageManagerComponent implements OnInit {
  langList: any;
  currentLang: string;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.currentLang = this.translate.getDefaultLang();
    this.langList = this.translate.getLangs();
  }
  
  changeLang() {
    this.translate.setDefaultLang(this.currentLang);
  }
}
