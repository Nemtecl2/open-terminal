import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import { TerminalPreferencesService } from '../../providers/terminal-preferences.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-terminal-opener',
  templateUrl: './terminal-opener.component.html',
  styleUrls: ['./terminal-opener.component.scss']
})
export class TerminalOpenerComponent implements OnInit, OnDestroy {
  @Output() load = new EventEmitter<any>();

  terminalListSubscription: Subscription;
  terminalList: any;
  currentTerminalSubscription: Subscription;
  currentTerminal: {
    name: string,
    cmd: string
  };

  constructor(private electronService: ElectronService,
    private terminalPreferencesService: TerminalPreferencesService,
    private translate: TranslateService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.currentTerminalSubscription = this.terminalPreferencesService.currentTerminalSubject.subscribe(
      (currentTerminal: any) => {
        this.currentTerminal = currentTerminal;
        this.load.emit(false);
      }
    );
    this.terminalPreferencesService.emitCurrentTerminalSubject();

    this.terminalListSubscription = this.terminalPreferencesService.terminalListSubject.subscribe(
      (terminalList: any[]) => {
        this.terminalList = terminalList;
      }
    );
    this.terminalPreferencesService.emitTerminalListSubject();
  }

  openTerminal() {
    this.electronService.childProcess.exec(this.currentTerminal.cmd, (err) => {
      if (err) {
        this.toastr.error(this.translate.instant('UNKNOWN_TERMINAL') + this.currentTerminal.name, null, {
          onActivateTick: true
        });
      }
    });
  }

  setCurrentTerminal(event) {
    this.load.emit(true);
    this.terminalPreferencesService.setCurrentTerminal(event);
  }

  ngOnDestroy() {
    this.currentTerminalSubscription.unsubscribe();
    this.terminalListSubscription.unsubscribe();
  }
}
