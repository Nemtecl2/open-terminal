import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalOpenerComponent } from './terminal-opener.component';

describe('TerminalOpenerComponent', () => {
  let component: TerminalOpenerComponent;
  let fixture: ComponentFixture<TerminalOpenerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminalOpenerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminalOpenerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
