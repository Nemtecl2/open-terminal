import { OpenTerminalPage } from './app.po';
import { browser, element, by } from 'protractor';

describe('aopen-terminal App', () => {
  let page: OpenTerminalPage;

  beforeEach(() => {
    page = new OpenTerminalPage();
  });

  it('should display message saying App works !', () => {
    page.navigateTo('/');
    expect(element(by.css('app-home h1')).getText()).toMatch('App works !');
  });
});
